import os
import ConfigParser
import openpyxl
from openpyxl import Workbook
from openpyxl.utils.cell import get_column_letter

config = ConfigParser.RawConfigParser()
config.read('defaults.cfg')

# From Config File
INPUT_WORKBOOK = config.get('excel', 'input_workbook')
SHEET_1 = config.get('excel', 'sheet_1')
SHEET_2 = config.get('excel', 'sheet_2')
COPY_COUNT = config.getint('excel', 'copy_count')
RESULT_SHEET = config.get('excel', 'result_sheet')
OUTPUT_WORKBOOK = config.get('excel', 'output_workbook')
REMOVE_INPUT_SHEETS = config.get('excel', 'remove_input_sheets')

def excel():
    workbook = openpyxl.load_workbook(INPUT_WORKBOOK)
    print "Initialized workbook. Ignore any warning message above."

    sheet_list = workbook.get_sheet_names()
    #print "Sheet Names: " , sheet_list

    sheet_1 = workbook.get_sheet_by_name(SHEET_1)
    sheet_2 = workbook.get_sheet_by_name(SHEET_2)

    if SHEET_1 not in sheet_list:
        print "Sheet {} does not exist in this workbook".format(SHEET_1)
        return

    if SHEET_2 not in sheet_list:
        print "Sheet {} does not exist in this workbook".format(SHEET_2)
        return

    row_count_sheet_1 = sheet_1.max_row
    col_count_sheet_1 = sheet_1.max_column
    print "There are {0} rows and {1} columns in {2}".format(row_count_sheet_1, row_count_sheet_1, sheet_1)

    row_count_sheet_2 = sheet_2.max_row
    col_count_sheet_2 = sheet_2.max_column
    print "There are {0} rows and {1} columns in {2}".format(row_count_sheet_2, row_count_sheet_2, sheet_2)

    if COPY_COUNT <= row_count_sheet_2:
        print "{0} rows will be copied from sheet {1} into sheet {2}".format(COPY_COUNT, sheet_2, sheet_1)
    else:
        print "WTF! There are only {0} rows in {1} and you want to copy {2} rows".format(row_count_sheet_2, SHEET_2, COPY_COUNT)
        return

    # Create output worksheet
    workbook.create_sheet(index=0, title=RESULT_SHEET)
    result_sheet = workbook.get_sheet_by_name(RESULT_SHEET)
    #print "Output sheet: ", result_sheet

    sheet1_start_cell = 'A1'
    sheet1_end_cell = str(get_column_letter(col_count_sheet_1)) + str(row_count_sheet_1)

    sheet2_start_cell = 'A1'
    sheet2_end_cell = str(get_column_letter(col_count_sheet_2)) + str(row_count_sheet_2)

    result_col = 1
    result_row = 1
    for row in sheet_1[sheet1_start_cell: sheet1_end_cell]:
        for cell in row:
            result_sheet[str(get_column_letter(result_col)) + str(result_row)] = cell.value
            result_col = result_col + 1

        result_row = result_row + 1
        result_col = 1

        for row2 in sheet_2[sheet2_start_cell: sheet2_end_cell]:
            for cell in row2:
                result_sheet[str(get_column_letter(result_col)) + str(result_row)] = cell.value
                result_col = result_col + 1

            result_row = result_row + 1
            result_col = 1

    if REMOVE_INPUT_SHEETS:
        workbook.remove_sheet(sheet_1)
        workbook.remove_sheet(sheet_2)

    workbook.save(OUTPUT_WORKBOOK)
    print "Output workbook saved: ", OUTPUT_WORKBOOK

if __name__ == '__main__':
    excel()
