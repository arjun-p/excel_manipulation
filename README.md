# What it does

Copy X number of rows from Sheet A and insert it after each row in Sheet B.

Input Sheet A:  
1  
2  
3  

Input Sheet B:  
a  
b  
c  

Output Sheet with X = 2:   
**a**  
1  
2  
**b**  
1  
2  
**c**  
1  
2  

# To get started

1) Install openpyxl:
   pip install openpyxl
   
2) Update defaults.cfg

3) Run python excel.py